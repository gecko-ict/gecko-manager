import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Grid } from 'semantic-ui-react';
import AuthService from '../service/authService';
import NavigationContainer from '../container/navigationContainer';
import MenuContainer from '../container/menuContainer';

class DashboardPage extends Component {

    authService = AuthService.getInstance();

    render() {
        return !this.authService.isAuthenticated() ? (
            <Redirect to="/login"/>
        ) : (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={3}>
                                <MenuContainer />
                            </Grid.Column>
                            <Grid.Column width={13}>
                                
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default DashboardPage;