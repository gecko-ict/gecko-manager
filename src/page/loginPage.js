import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Grid, Segment, Input, Button } from 'semantic-ui-react';
import NavigationContainer from '../container/navigationContainer';
import AuthService from '../service/authService';
import NotificationManager from '../helper/notificationManager';

class LoginPage extends Component {
    
    authService = AuthService.getInstance();
    notificationManager = NotificationManager.getInstance();
    
    state = {
        username: '',
        password: ''
    };

    isValidCredentials() {
        if (this.state.username === '') {
            this.notificationManager.error('Username field can\'t be blank');
            return false;
        }
        if (this.state.password === '') {
            this.notificationManager.error('Password field can\'t be blank');
            return false;
        }
        return true;
    }

    handleEnter(event) {
        if(event.key === 'Enter'){
            this.loginAction();
        }
    }

    loginAction() {
        if (this.isValidCredentials()) {
            this.authService.login(this.state.username, this.state.password).catch((response) => {
                this.notificationManager.error(response.message);
            })
        }
    }

    render() {
        return this.authService.isAuthenticated() ? (
            <Redirect to="/dashboard"/>
        ) : (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={5}>
                                <Segment>
                                    <Input onChange={(e, data) => this.setState({username: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                    value={this.state.username} fluid type='text' icon='user' iconPosition='left' placeholder='Username' />
                                    <br />
                                    <Input onChange={(e, data) => this.setState({password: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                    value={this.state.password} fluid type='password' icon='key' iconPosition='left' placeholder='Password' />
                                    <br />
                                    <br />
                                    <Button onClick={() => this.loginAction()} fluid secondary>Login</Button>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        );
    }
}

export default LoginPage;
