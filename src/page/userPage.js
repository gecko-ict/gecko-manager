import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Grid, Image, Header, Label } from 'semantic-ui-react';
import AuthService from '../service/authService';
import NavigationContainer from '../container/navigationContainer';
import MenuContainer from '../container/menuContainer';
import ProfileInfoContainer from '../container/profileInfoContainer';
import UserService from '../service/userService';
import NotificationManager from '../helper/notificationManager';
import History from '../helper/history';

class UserPage extends Component {
    
    state = {
        data: {
            info: {}
        }
    };

    authService = AuthService.getInstance();
    userService = UserService.getInstance();
    history = History.getInstance();
    notificationManager = NotificationManager.getInstance();

    componentDidMount() {
        if (this.isCurrentUser()) {
            this.setState({
                data: this.authService.user
            });
        } else {
            this.fetchUser();
        }
    }

    isCurrentUser() {
        return this.props.match.params.username === this.authService.user.username;
    }

    fetchUser() {
        this.userService.getUser(this.props.match.params.username).then((response) => {
            console.log('success', response);
            this.setState({
                data: response.data
            });
        }).catch((response) => {
            if (response.code !== 'unauthorized') {
                this.history.forward('/');
                this.notificationManager.error(response.message);
            }
        });
    }

    render() {
        return !this.authService.isAuthenticated() ? (
            <Redirect to="/login"/>
        ) : (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={2}>
                                <MenuContainer />
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Header as='h2'>
                                    {this.state.data.info.fullname} - 
                                    <Label color='teal'>{this.state.data.role}</Label>
                                </Header>
                                <Container>
                                    <Grid>
                                        <Grid.Row>
                                            <Grid.Column width={12}>
                                                <ProfileInfoContainer data={this.state.data} />
                                            </Grid.Column>
                                            <Grid.Column width={4}>
                                                <Image centered size='medium' src='/static/avatar.png' />
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Container>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default UserPage;