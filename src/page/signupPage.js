import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Grid, Segment, Input, Button, Checkbox } from 'semantic-ui-react';
import NavigationContainer from '../container/navigationContainer';
import AuthService from '../service/authService';
import NotificationManager from '../helper/notificationManager';
import mailValidator from '../helper/mailValidator';

class SignupPage extends Component {

    authService = AuthService.getInstance();
    notificationManager = NotificationManager.getInstance();

    state = {
        fullname: '',
        username: '',
        mail: '',
        password: ''
    };

    isValidCredentials() {
        if (this.state.fullname === '') {
            this.notificationManager.error('Full name field can\'t be blank');
            return false;
        }
        if (this.state.username === '') {
            this.notificationManager.error('Username field can\'t be blank');
            return false;
        }
        if (this.state.mail === '') {
            this.notificationManager.error('Email field can\'t be blank');
            return false;
        }
        if (this.state.password === '') {
            this.notificationManager.error('Password field can\'t be blank');
            return false;
        }
        if(!mailValidator(this.state.mail)) {
            this.notificationManager.error('Inserted Email is not valid');
            return false;
        }
        if(this.state.password.length < 6) {
            this.notificationManager.error('Password length must be bigger than 6 characters');
            return false;
        }
        return true;
    }

    handleEnter(event) {
        if(event.key === 'Enter'){
            this.signupAction();
        }
    }

    signupAction() {
        if (this.isValidCredentials()) {
            this.authService.register(this.state.username, this.state.mail, this.state.password, this.state.fullname).catch((response) => {
                this.notificationManager.error(response.message);
            })
        }
    }

    render() {
        return this.authService.isAuthenticated() ? (
            <Redirect to="/dashboard"/>
        ) : (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={5}>
                                <Segment>
                                    <Input onChange={(e, data) => this.setState({fullname: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                        value={this.state.fullname} fluid type='text' icon='hashtag' iconPosition='left'
                                        placeholder='Full Name' />
                                    <br />
                                    <Input onChange={(e, data) => this.setState({username: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                        value={this.state.username} fluid type='text' icon='user' iconPosition='left'
                                        placeholder='Username' />
                                    <br />
                                    <Input onChange={(e, data) => this.setState({mail: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                        value={this.state.mail} fluid type='text' icon='at' iconPosition='left'
                                        placeholder='Email' />
                                    <br />
                                    <Input onChange={(e, data) => this.setState({password: data.value})} onKeyPress={(e) => this.handleEnter(e)}
                                        value={this.state.password} fluid type='password' icon='key' iconPosition='left'
                                        placeholder='Password' />
                                    <br />
                                    <Checkbox label='I agree with terms of use' />
                                    <br />
                                    <br />
                                    <Button onClick={() => this.signupAction()} fluid secondary>Sign Up</Button>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        );
    }
}

export default SignupPage;
