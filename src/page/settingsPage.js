import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Grid, Tab } from 'semantic-ui-react';
import AuthService from '../service/authService';
import NavigationContainer from '../container/navigationContainer';
import MenuContainer from '../container/menuContainer';
import GeneralSettingsContainer from '../container/generalSettingsContainer';
class SettingsPage extends Component {

    authService = AuthService.getInstance();

    render() {
        const panes = [
            { menuItem: 'General', render: () => <GeneralSettingsContainer />}
        ];
        return !this.authService.isAuthenticated() ? (
            <Redirect to="/login"/>
        ) : (
            <div>
                <NavigationContainer />
                <Container className="page">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={2}>
                                <MenuContainer />
                            </Grid.Column>
                            <Grid.Column width={14}>
                                <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default SettingsPage;