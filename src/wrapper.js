import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import { ToastContainer } from 'react-toastify';

import History from './helper/history';

import IndexPage from './page/indexPage';
import DashboardPage from './page/dashboardPage';
import UserPage from './page/userPage';
import LoginPage from './page/loginPage';
import SignupPage from './page/signupPage';
import SettingsPage from './page/settingsPage';
import PanelPage from './page/panelPage';
import NotFoundPage from './page/notFoundPage';

class Wrapper extends Component {
    
    history = History.getInstance();

    render() {
        return (
            <div>
                <ToastContainer />
                <Router history={this.history.api}>
                    <Switch>
                        <Route exact path="/" component={IndexPage} />
                        <Route exact path="/login" component={LoginPage} />
                        <Route exact path="/signup" component={SignupPage} />
                        <Route exact path="/dashboard" component={DashboardPage} />
                        <Route exact path="/user/:username" component={UserPage} />
                        <Route exact path="/settings" component={SettingsPage} />
                        <Route exact path="/panel" component={PanelPage} />
                        <Route exact component={NotFoundPage} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default Wrapper;