import React, { Component } from 'react';
import { Container, Grid, Table, Icon, Button, Menu, Label } from 'semantic-ui-react';

class UserPanelContainer extends Component {

    render() {
        return (
            <Container>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Button icon secondary size='small'><Icon name='user' /> Add User</Button>
                            <Table celled compact>
                                <Table.Header fullWidth>
                                    <Table.Row>
                                        <Table.HeaderCell>Username</Table.HeaderCell>
                                        <Table.HeaderCell>Full name</Table.HeaderCell>
                                        <Table.HeaderCell>E-mail address</Table.HeaderCell>
                                        <Table.HeaderCell>Role</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                            
                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell>Jill Lewis</Table.Cell>
                                        <Table.Cell>May 11, 2014</Table.Cell>
                                        <Table.Cell>jilsewris22@yahoo.com</Table.Cell>
                                        <Table.Cell><Label color='teal'>VISITOR</Label></Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            
                                <Table.Footer fullWidth>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='4'>
                                            <Menu floated='right' pagination>
                                                <Menu.Item as='a' icon>
                                                <Icon name='left chevron' />
                                                </Menu.Item>
                                                <Menu.Item as='a'>1</Menu.Item>
                                                <Menu.Item as='a'>2</Menu.Item>
                                                <Menu.Item as='a'>3</Menu.Item>
                                                <Menu.Item as='a'>4</Menu.Item>
                                                <Menu.Item as='a' icon>
                                                <Icon name='right chevron' />
                                                </Menu.Item>
                                            </Menu>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

export default UserPanelContainer;