import React, { Component } from 'react';
import { Icon, Menu } from 'semantic-ui-react'
import History from '../helper/history';

class MenuContainer extends Component {
    
    history = History.getInstance();
    
    handleItemClick(e, data) {
        this.history.forward('/' + data.name);
    }

    render() {
        return (
            <Menu compact icon='labeled' vertical>
                <Menu.Item name='dashboard' onClick={(e, data) => this.handleItemClick(e, data)}>
                <Icon name='dashboard' />Dashboard</Menu.Item>

                <Menu.Item name='messenger' onClick={(e, data) => this.handleItemClick(e, data)}>
                <Icon name='comments' />Messenger</Menu.Item>

                <Menu.Item name='tracker' onClick={(e, data) => this.handleItemClick(e, data)}>
                <Icon name='browser' />Issue tracker</Menu.Item>

                <Menu.Item name='meetup' onClick={(e, data) => this.handleItemClick(e, data)}>
                <Icon name='meetup' />Meetup</Menu.Item>

                <Menu.Item name='panel' onClick={(e, data) => this.handleItemClick(e, data)}>
                <Icon name='settings' />Panel</Menu.Item>
            </Menu>
        );
    }
}

export default MenuContainer;