import React, { Component } from 'react';
import { Container, Grid, Header } from 'semantic-ui-react';

class GeneralSettingsContainer extends Component {

    render() {
        return (
            <Container>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Header color='green'>TODO: edit fields for user personal data</Header>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

export default GeneralSettingsContainer;