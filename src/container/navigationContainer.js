import React, { Component } from 'react';
import { Container, Menu, Button } from 'semantic-ui-react';
import History from '../helper/history';
import AuthService from '../service/authService';
import UserDropdown from '../component/userDropdown';

class NavigationContainer extends Component {
    
    state = {
        username: '',
        fullname: ''
    };

    history = History.getInstance();
    authService = AuthService.getInstance();

    render() {
        
        return (
            <div className="navigation-container">
                <Container>
                    <Menu secondary borderless className="navigation" color="black">
                        <Menu.Item className="logo">
                            <img alt="logo" src="/static/logo.png" />
                        </Menu.Item>
                            {!this.authService.isAuthenticated() ? (
                                <Menu.Menu position='right'>
                                    <Menu.Item>
                                        <Button onClick={() => {
                                            this.history.forward('/signup');
                                        }} secondary>Sign Up</Button>
                                    </Menu.Item>
                                    <Menu.Item>
                                        <Button color='green' onClick={() => {
                                            this.history.forward('/login');
                                        }}>Login</Button>
                                    </Menu.Item>
                                </Menu.Menu>
                            ) : (
                                <Menu.Menu position='right'>
                                    <Menu.Item>
                                        <UserDropdown />
                                    </Menu.Item>
                                </Menu.Menu>
                            )}
                    </Menu>
                </Container>
            </div>
        );
    }
}

export default NavigationContainer;
