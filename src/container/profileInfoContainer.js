import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';

class ProfileInfoContainer extends Component {

    render() {
        return (
            <Table basic>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Info</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    <Table.Row>
                        <Table.Cell>Username</Table.Cell>
                        <Table.Cell>{this.props.data.username}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Mail</Table.Cell>
                        <Table.Cell>{this.props.data.mail}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Last Login Date</Table.Cell>
                        <Table.Cell>{this.props.data.lastLogin}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Age</Table.Cell>
                        <Table.Cell>{this.props.data.info.age}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Ocupation</Table.Cell>
                        <Table.Cell>{this.props.data.info.ocupation}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Company</Table.Cell>
                        <Table.Cell>{this.props.data.info.company}</Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        );
    }
}

export default ProfileInfoContainer;