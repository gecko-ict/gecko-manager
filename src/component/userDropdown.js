import React, { Component } from 'react';
import { Dropdown, Icon } from 'semantic-ui-react';
import History from '../helper/history';
import AuthService from '../service/authService';

class UserDropdown extends Component {
    
    history = History.getInstance();
    authService = AuthService.getInstance();

    action = {
        'profile': () => this.history.forward('/user/' + this.authService.user.username),
        'settings': () => this.history.forward('/settings'),
        'signout': () => this.authService.logout()
    };
    
    getOptions() {
        let options = [
            {
              key: 'user',
              text: <span>Signed in as <strong>{this.authService.user.username}</strong></span>,
              disabled: true,
            },
            { value: 'null', text: '' },
            { value: 'profile', text: 'Your Profile' },
        ];

        options.push(
            { value: 'settings', text: 'Settings' },
            { value: 'signout', text: 'Sign Out' }
        );

        return options;
    }

    render() {
        return (
            <Dropdown onChange={(e, data) => {
                if (typeof this.action[data.value] !== 'undefined') {
                    this.action[data.value].call(this, data.value);
                }
            }} trigger={
                <span>
                    <Icon name='user' /> Hello, {this.authService.user.info.fullname.split(" ")[0]}
                </span>
                } options={this.getOptions()} />
        );
    }
}

export default UserDropdown;