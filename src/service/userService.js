import Api from '../helper/api';

class UserService {

    api = Api.getInstance();

    getUser(username) {
        return this.api.fetch('GET', `/api/user/${username}`);
    }

}  

var instance = null;
UserService.getInstance = function() {
    if (instance === null) {
        instance = new UserService();
    }
    return instance;
};

export default UserService;