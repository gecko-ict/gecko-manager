import Api from '../helper/api';
import History from '../helper/history';
import NotificationManager from '../helper/notificationManager';

class AuthService {

    authenticated = false;
    user = null;

    api = Api.getInstance();
    history = History.getInstance();
    notificationManager = NotificationManager.getInstance();

    constructor() {
        this.api.onUnautorized.add(this.onUnautorized, this);
        let data = null;
        try {
            data = JSON.parse(atob(localStorage.getItem(btoa('user:auth'))));
        } catch (error) {}
        if (data !== null) {
            this.authenticated = true;
            this.user = data;
            this.api.addToken(data.token);
        }
    }

    login(username, password) {
        return this.api.fetch('GET', `/api/login?username=${username}&password=${password}`).then((response) => {
            this.authenticate(response.data);
            return response;
        });
    }

    logout() {
        localStorage.removeItem(btoa('user:auth'));
        this.authenticated = false;
        this.user = null;
        this.api.removeToken();
        this.history.forward('/');
    }

    register(username, mail, password, fullname) {
        return this.api.fetch('POST', '/api/signup', {
            data: { username, mail, password, fullname }
        }).then((response) => {
            this.authenticate(response.data);
            return response;
        });
    }

    authenticate(data) {
        localStorage.setItem(btoa('user:auth'), btoa(JSON.stringify(data)));
        this.authenticated = true;
        this.user = data;
        this.api.addToken(data.token);
        this.history.forward('/');
        this.notificationManager.success('Successfully logged in!');
    }

    onUnautorized() {
        this.logout();
        this.notificationManager.warning('Session expired!');
    }

    isAuthenticated() {
        return this.authenticated;
    }
}

var instance = null;
AuthService.getInstance = function() {
    if (instance === null) {
        instance = new AuthService();
    }
    return instance;
}

export default AuthService;