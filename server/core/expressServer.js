import express from 'express';
import ws from 'express-ws';
import cors from 'cors';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';
import DEV_CONFIG from '../config/config.dev';
import PROD_CONFIG from '../config/config.prod';

import Logger from '../helper/logger';

class ExpressServer {

    logger = Logger.getInstance();
    express = ws(express());
    controllerMap = {};

    constructor() {
        this.express.app.use(cors());
        this.express.app.use(bodyParser.json());
        this.express.app.disable('x-powered-by');
        
        let ENV = null;
        let DB_CONFIG = null;
        let fileData = null;

        if (process.env['ENV'] === 'DEV') {
            ENV = DEV_CONFIG;
            try {
                fileData = fs.readFileSync(path.join(__dirname, '../../.env.database.local'));
                DB_CONFIG = dotenv.parse(fileData);
            } catch (error) {
                try {
                    fileData = fs.readFileSync(path.join(__dirname, '../../.env.database'));
                    DB_CONFIG = dotenv.parse(fileData);
                } catch (error) {
                    this.logger.error('create .env.database configuration');    
                }   
            }
        } else {
            ENV = PROD_CONFIG;
            try {
                fileData = fs.readFileSync(path.join(__dirname, '../../../.env.database'));
                DB_CONFIG = dotenv.parse(fileData);
            } catch (error) {
                this.logger.error('create .env.database configuration');    
            } 
        }

        process.env['ROOT'] = path.join(__dirname, '../');
        process.env['PORT'] = process.env['PORT'] || ENV['PORT'];
        process.env['WEBAPP_INDEX'] = ENV['WEBAPP_INDEX'];
        process.env['WEBAPP_STATIC'] = ENV['WEBAPP_STATIC'];

        process.env['DB_HOST'] = DB_CONFIG['DB_HOST'];
        process.env['DB_USER'] = DB_CONFIG['DB_USER'];
        process.env['DB_PASSWORD'] = DB_CONFIG['DB_PASSWORD'];
        process.env['DB_NAME'] = DB_CONFIG['DB_NAME'];

        // additional 
        process.env['SENDGRID_API_KEY'] = process.env['SENDGRID_API_KEY'] || ENV['SENDGRID_API_KEY'];

    }

    addMiddleware(route, middleware) {
        this.express.app.use(route, middleware);
    }

    addController(route, controller) {
        this.controllerMap[route] = new controller(this.express, route);
    }

    addStatic(route, dir, options = {}) {
        this.express.app.use(route, express.static(path.join(__dirname, path.join('../', dir)), options));
    }

    run() {
        this.express.app.listen(process.env['PORT']);
        this.logger.info('Server started on port ' + process.env['PORT']);
    }
}

export default ExpressServer;