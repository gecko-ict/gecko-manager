import mysql from 'mysql';
import Logger from '../helper/logger';

class MySQL {

    logger = Logger.getInstance();
    pool = null;

    constructor() {
        this.pool = mysql.createPool({
            connectionLimit : 5,
            host            : process.env['DB_HOST'],
            user            : process.env['DB_USER'],
            password        : process.env['DB_PASSWORD'],
            database        : process.env['DB_NAME']
        });
        this.query('SELECT 1 + 1;').then(() => {
            this.logger.debug('Database pool is ready!');
        }).catch((error) => {
            this.logger.error('Something went wrong with database connection!', error);
        });
    }

    query(query, params = []) {
        return new Promise((resolve, reject) => {
            this.pool.query(query, params, function (error, results, fields) {
                if (error) {
                    this.logger.error('DATABASE: ', error);
                } else {
                    resolve({
                        results: results,
                        fields: fields
                    });
                } 
            });
        });
    }
}

var instance = null;
MySQL.getInstance = function() {
    if (instance === null) {
        instance = new MySQL();
    }
    return instance;
};

export default MySQL;