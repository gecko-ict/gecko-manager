import { createClient } from 'then-redis'
import Logger from '../../helper/logger';

class Redis {
    
    client = null;
    logger = Logger.getInstance();

    constructor() {
        this.client = createClient(process.env.REDIS_URL);
        this.client.on('error', this._onError);
    }

    _onError = (err) => {
        this.logger.error('Caching System: ' + err);
    }
}

var instance = null;
Redis.getInstance = function() {
    if (instance === null) {
        instance = new Redis();
    }
    return instance;
}

export default Redis;