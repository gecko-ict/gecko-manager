import sendgrid from 'sendgrid'
import Logger from '../../helper/logger';

class SendGridMail {
    
    helper = sendgrid.mail;
    client = sendgrid(process.env.SENDGRID_API_KEY);
    logger = Logger.getInstance();

    sendMail(mailFrom, mailTo, subject, contentType, content) {
        let mail = this.helper.Mail(
            new this.helper.Email(mailFrom),
            new this.helper.Email(mailTo),
            subject,
            new this.helper.Content(contentType, content)
        );
        let request = this.client.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });
        return new Promise((resolve, reject) => {
            this.client.API(request, (error, response) => {
                if (error) {
                    reject();
                } else {
                    resolve();
                }
            });
        });
    }

    _onError = (err) => {
        this.logger.error('Caching System: ' + err);
    }
}

var instance = null;
SendGridMail.getInstance = function() {
    if (instance === null) {
        instance = new SendGridMail();
    }
    return instance;
}

export default SendGridMail;