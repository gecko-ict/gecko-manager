import Controller from '../core/controller';
import Logger from '../helper/logger';
import UserService from '../service/userService';

class UserController extends Controller {

    get(request, response) {
        let username = request.params.username;

        let userService = new UserService();
        let logger = Logger.getInstance();
        
        userService.getUser(username).then((responseDto) => {
            response.status(200).json(responseDto);
        }).catch((responseDto) => {
            console.log(responseDto, this);
            logger.error('[GET:/user/:username]', responseDto);
            response.status(200).json(responseDto);
        });
    }

    put() {

    }

    delete() {

    }
}

export default UserController;