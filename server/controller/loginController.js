import Controller from '../core/controller';
import Logger from '../helper/logger';
import UserService from '../service/userService';

class LoginController extends Controller {
    
    get(request, response) {
        let username = request.query.username;
        let password = request.query.password;

        let logger = Logger.getInstance();
        let userService = new UserService();
        
        userService.login(username, password).then((responseDto) => {
            response.status(200).json(responseDto);
        }).catch((responseDto) => {
            logger.error('[GET:/login]', responseDto);
            response.status(200).json(responseDto);
        });
    }
    
}

export default LoginController;