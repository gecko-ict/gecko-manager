class UserDto {
    username = null;
    mail = null;
    role = null;
    lastLogin = null;
    token = null;
    info = {
        fullname: null,
        age: null,
        ocupation: null,
        company: null
    };
    settings = {
        infoWizzard: false
    };
}

export default UserDto;