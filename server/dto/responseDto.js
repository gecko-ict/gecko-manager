class ResponseDto {
    status = 200;
    code = '';
    message = '';
    data = null;
}

export default ResponseDto;