export default [
    {
        path: '/',
        code: 200
    }, {
        path: '/login',
        code: 200
    }, {
        path: '/signup',
        code: 200
    }, {
        path: '/dashboard',
        code: 200
    }
];