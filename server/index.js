import ExpressServer from './core/expressServer';

import RootController from './controller/rootController';
import LoginController from './controller/loginController';
import SignupController from './controller/signupController';
import UserController from './controller/userController';

import authMiddleware from './middleware/authMiddleware';

let server = new ExpressServer();
server.addStatic('/static', process.env.WEBAPP_STATIC);

server.addMiddleware('/api/user/:username', authMiddleware());

server.addController('/api/login', LoginController);
server.addController('/api/signup', SignupController);
server.addController('/api/user/:username', UserController);
server.addController('/*', RootController);

server.run();