'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redis = require('../core/adapter/redis');

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserDao = function () {
    function UserDao() {
        _classCallCheck(this, UserDao);

        this.redis = _redis2.default.getInstance();
    }

    _createClass(UserDao, [{
        key: 'get',
        value: function get(id) {
            return this.redis.client.get('user:' + id).then(function (value) {
                return JSON.parse(value);
            });
        }
    }, {
        key: 'create',
        value: function create(user) {
            var _this = this;

            return this.redis.client.get('user:' + user.id).then(function (value) {
                if (value !== null) throw new Error('user_exist');
                return _this.redis.client.get('user:username:' + user.username);
            }).then(function (value) {
                if (value !== null) throw new Error('username_exist');
                return _this.redis.client.get('user:mail:' + user.mail);
            }).then(function (value) {
                if (value !== null) throw new Error('mail_exist');
                return _this.redis.client.set('user:' + user.id, JSON.stringify(user));
            }).then(function (value) {
                _this.redis.client.set('user:username:' + user.username, user.id);
                _this.redis.client.set('user:mail:' + user.mail, user.id);
                return user;
            });
        }
    }, {
        key: 'update',
        value: function update(user) {
            var _this2 = this;

            var oldUser = null;
            return this.delete(user.id).then(function (value) {
                return _this2.create('user:' + user.id, JSON.stringify(user));
            });
        }
    }, {
        key: 'delete',
        value: function _delete(id) {
            var _this3 = this;

            return this.redis.client.del('user:' + id).then(function () {
                return _this3.redis.client.del('user:username:' + id);
            }).then(function () {
                return _this3.redis.client.del('user:mail:' + id);
            });
        }
    }, {
        key: 'getByUsername',
        value: function getByUsername(username) {
            var _this4 = this;

            return this.redis.client.get('user:username:' + username).then(function (value) {
                if (value === null) return null;else return _this4.get(value);
            });
        }
    }, {
        key: 'getByMail',
        value: function getByMail(mail) {
            var _this5 = this;

            return this.redis.client.get('user:mail:' + username).then(function (value) {
                if (value === null) return null;else return _this5.get(value);
            });
        }
    }]);

    return UserDao;
}();

exports.default = UserDao;
//# sourceMappingURL=userDao.js.map