'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _userService = require('../service/userService');

var _userService2 = _interopRequireDefault(_userService);

var _responseDto = require('../dto/responseDto');

var _responseDto2 = _interopRequireDefault(_responseDto);

var _logger = require('../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var userRoleList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    return function (request, response, next) {
        var token = request.get('Authorization');
        var userService = new _userService2.default();
        var responseDto = new _responseDto2.default();
        var logger = _logger2.default.getInstance();
        userService.canAccess(token, userRoleList).then(function (can) {
            if (can) {
                next();
            } else {
                responseDto.status = 401;
                responseDto.code = 'unauthorized';
                responseDto.message = 'unauthorized'; // TODO: implement error messages
                response.status(200).json(responseDto);
            }
        }).catch(function (error) {
            logger.error('[AUTH MIDDLEWARE]', error);
            responseDto.status = 500;
            responseDto.code = 'internal_server_error';
            responseDto.message = error.message; // TODO: implement error messages
            response.status(200).json(responseDto);
        });
    };
};
//# sourceMappingURL=authMiddleware.js.map