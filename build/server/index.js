'use strict';

var _expressServer = require('./core/expressServer');

var _expressServer2 = _interopRequireDefault(_expressServer);

var _rootController = require('./controller/rootController');

var _rootController2 = _interopRequireDefault(_rootController);

var _loginController = require('./controller/loginController');

var _loginController2 = _interopRequireDefault(_loginController);

var _signupController = require('./controller/signupController');

var _signupController2 = _interopRequireDefault(_signupController);

var _userController = require('./controller/userController');

var _userController2 = _interopRequireDefault(_userController);

var _authMiddleware = require('./middleware/authMiddleware');

var _authMiddleware2 = _interopRequireDefault(_authMiddleware);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = new _expressServer2.default();
server.addStatic('/static', process.env.WEBAPP_STATIC);

server.addMiddleware('/api/user/:username', (0, _authMiddleware2.default)());

server.addController('/api/login', _loginController2.default);
server.addController('/api/signup', _signupController2.default);
server.addController('/api/user/:username', _userController2.default);
server.addController('/*', _rootController2.default);

server.run();
//# sourceMappingURL=index.js.map