'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _controller = require('../core/controller');

var _controller2 = _interopRequireDefault(_controller);

var _logger = require('../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

var _userService = require('../service/userService');

var _userService2 = _interopRequireDefault(_userService);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserController = function (_Controller) {
    _inherits(UserController, _Controller);

    function UserController() {
        _classCallCheck(this, UserController);

        return _possibleConstructorReturn(this, (UserController.__proto__ || Object.getPrototypeOf(UserController)).apply(this, arguments));
    }

    _createClass(UserController, [{
        key: 'get',
        value: function get(request, response) {
            var _this2 = this;

            var username = request.params.username;

            var userService = new _userService2.default();
            var logger = _logger2.default.getInstance();

            userService.getUser(username).then(function (responseDto) {
                response.status(200).json(responseDto);
            }).catch(function (responseDto) {
                console.log(responseDto, _this2);
                logger.error('[GET:/user/:username]', responseDto);
                response.status(200).json(responseDto);
            });
        }
    }, {
        key: 'put',
        value: function put() {}
    }, {
        key: 'delete',
        value: function _delete() {}
    }]);

    return UserController;
}(_controller2.default);

exports.default = UserController;
//# sourceMappingURL=userController.js.map