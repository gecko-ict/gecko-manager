"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserDto = function UserDto() {
    _classCallCheck(this, UserDto);

    this.id = null;
    this.username = null;
    this.mail = null;
    this.password = null;
    this.role = null;
    this.lastLogin = null;
    this.info = {
        fullname: null,
        age: null,
        ocupation: null,
        company: null
    };
    this.settings = {
        infoWizzard: false
    };
};

exports.default = UserDto;
//# sourceMappingURL=user.js.map