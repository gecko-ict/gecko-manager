'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _md = require('md5');

var _md2 = _interopRequireDefault(_md);

var _logger = require('../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

var _responseDto = require('../dto/responseDto');

var _responseDto2 = _interopRequireDefault(_responseDto);

var _userDto = require('../dto/userDto');

var _userDto2 = _interopRequireDefault(_userDto);

var _user = require('../model/user');

var _user2 = _interopRequireDefault(_user);

var _userDao = require('../dao/userDao');

var _userDao2 = _interopRequireDefault(_userDao);

var _userTokenDao = require('../dao/userTokenDao');

var _userTokenDao2 = _interopRequireDefault(_userTokenDao);

var _userRoleEnum = require('../enum/userRoleEnum');

var _userRoleEnum2 = _interopRequireDefault(_userRoleEnum);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserService = function () {
    function UserService() {
        _classCallCheck(this, UserService);

        this.userDao = new _userDao2.default();
        this.userTokenDao = new _userTokenDao2.default();
        this.defaultRoleMask = [_userRoleEnum2.default.VISITOR];
    }

    _createClass(UserService, [{
        key: 'signup',
        value: function signup(username, mail, password, fullname) {
            var _this = this;

            return new Promise(function (resolve, reject) {
                var responseDto = new _responseDto2.default();
                if (typeof username === 'undefined' || typeof mail === 'undefined' || typeof password === 'undefined' || typeof fullname === 'undefined') {
                    responseDto.status = 412;
                    responseDto.code = 'missing_parameter';
                    responseDto.message = 'missing_parameter'; // TODO: create error code table
                    resolve(responseDto);
                } else {
                    _this._createUser(username, mail, password, fullname).then(function (user) {
                        var token = _this._createToken();
                        var userDto = _this._createUserDto(user, token);
                        _this.userTokenDao.create(userDto);
                        return userDto;
                    }).then(function (userDto) {
                        responseDto.status = 201;
                        responseDto.data = userDto;
                        resolve(responseDto);
                    }).catch(function (error) {
                        responseDto.status = 500;
                        responseDto.code = error.message;
                        responseDto.message = error.message; // TODO: create error code table
                        reject(responseDto);
                    });
                }
            });
        }
    }, {
        key: 'login',
        value: function login(username, password) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                var responseDto = new _responseDto2.default();
                if (typeof username === 'undefined' || password === 'undefined') {
                    responseDto.status = 412;
                    responseDto.code = 'missing_parameter';
                    responseDto.message = 'missing_parameter'; // TODO: create error code table
                    resolve(responseDto);
                } else {
                    _this2.userDao.getByUsername(username).then(function (user) {
                        if (user === null) {
                            responseDto.status = 451;
                            responseDto.code = 'wrong_credentials';
                            responseDto.message = 'wrong_credentials'; // TODO: create error code table
                            resolve(responseDto);
                        } else if ((0, _md2.default)(password) === user.password) {
                            var token = _this2._createToken(user);
                            var userDto = _this2._createUserDto(user, token);
                            _this2.userTokenDao.create(userDto);
                            return userDto;
                        } else {
                            responseDto.status = 451;
                            responseDto.code = 'wrong_credentials';
                            responseDto.message = 'wrong_credentials'; // TODO: create error code table
                            resolve(responseDto);
                        }
                    }).then(function (userDto) {
                        responseDto.status = 200;
                        responseDto.data = userDto;
                        resolve(responseDto);
                    }).catch(function (error) {
                        responseDto.status = 500;
                        responseDto.code = error.message;
                        responseDto.message = error.message; // TODO: create error code table
                        resolve(responseDto);
                    });
                }
            });
        }
    }, {
        key: 'canAccess',
        value: function canAccess(token) {
            var userRoleList = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            return this.userTokenDao.get(token).then(function (userDto) {
                if (userDto === null) {
                    return false;
                }
                if (userRoleList === null) {
                    return true;
                } else {
                    var roleMask = _userRoleEnum2.default.get(userDto.role);
                    var _iteratorNormalCompletion = true;
                    var _didIteratorError = false;
                    var _iteratorError = undefined;

                    try {
                        for (var _iterator = userRoleList[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            var ROLE = _step.value;

                            if (roleMask.has(ROLE)) {
                                return true;
                            }
                        }
                    } catch (err) {
                        _didIteratorError = true;
                        _iteratorError = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion && _iterator.return) {
                                _iterator.return();
                            }
                        } finally {
                            if (_didIteratorError) {
                                throw _iteratorError;
                            }
                        }
                    }

                    return false;
                }
            });
        }
    }, {
        key: 'getUser',
        value: function getUser(username) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
                var responseDto = new _responseDto2.default();
                if (typeof username === 'undefined') {
                    responseDto.status = 412;
                    responseDto.code = 'missing_parameter';
                    reject(responseDto);
                } else {
                    _this3.userDao.getByUsername(username).then(function (user) {
                        if (user !== null) {
                            var token = null;
                            var userDto = _this3._createUserDto(user, token);
                            responseDto.status = 200;
                            responseDto.data = userDto;
                            resolve(responseDto);
                        } else {
                            console.log();
                            responseDto.status = 404;
                            responseDto.code = 'user_not_found';
                            responseDto.message = 'user_not_found'; // TODO: create error code table
                            resolve(responseDto);
                        }
                    }).catch(function (error) {
                        responseDto.status = 500;
                        responseDto.code = 'internal_server_error';
                        responseDto.message = error.message;
                        reject(responseDto);
                    });
                }
            });
        }
    }, {
        key: '_createToken',
        value: function _createToken() {
            return (0, _v2.default)();
        }
    }, {
        key: '_createUserDto',
        value: function _createUserDto(user, token) {
            var userDto = new _userDto2.default();
            userDto.username = user.username;
            userDto.mail = user.mail;
            userDto.role = user.role;
            userDto.lastLogin = user.lastLogin;
            userDto.info = user.info;
            userDto.settings = user.settings;
            userDto.token = token;
            return userDto;
        }
    }, {
        key: '_createUser',
        value: function _createUser(username, mail, password, fullname) {
            var user = new _user2.default();
            user.id = (0, _v2.default)();
            user.username = username;
            user.password = (0, _md2.default)(password);
            user.mail = mail;
            user.info.fullname = fullname;
            user.role = this.defaultRoleMask.join(' | ');
            return this.userDao.create(user);
        }
    }]);

    return UserService;
}();

exports.default = UserService;
//# sourceMappingURL=userService.js.map