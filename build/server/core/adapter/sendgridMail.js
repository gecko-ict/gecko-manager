'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _sendgrid = require('sendgrid');

var _sendgrid2 = _interopRequireDefault(_sendgrid);

var _logger = require('../../helper/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SendGridMail = function () {
    function SendGridMail() {
        var _this = this;

        _classCallCheck(this, SendGridMail);

        this.helper = _sendgrid2.default.mail;
        this.client = (0, _sendgrid2.default)(process.env.SENDGRID_API_KEY);
        this.logger = _logger2.default.getInstance();

        this._onError = function (err) {
            _this.logger.error('Caching System: ' + err);
        };
    }

    _createClass(SendGridMail, [{
        key: 'sendMail',
        value: function sendMail(mailFrom, mailTo, subject, contentType, content) {
            var _this2 = this;

            var mail = this.helper.Mail(new this.helper.Email(mailFrom), new this.helper.Email(mailTo), subject, new this.helper.Content(contentType, content));
            var request = this.client.emptyRequest({
                method: 'POST',
                path: '/v3/mail/send',
                body: mail.toJSON()
            });
            return new Promise(function (resolve, reject) {
                _this2.client.API(request, function (error, response) {
                    if (error) {
                        reject();
                    } else {
                        resolve();
                    }
                });
            });
        }
    }]);

    return SendGridMail;
}();

var instance = null;
SendGridMail.getInstance = function () {
    if (instance === null) {
        instance = new SendGridMail();
    }
    return instance;
};

exports.default = SendGridMail;
//# sourceMappingURL=sendgridMail.js.map